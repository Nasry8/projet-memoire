**Sujet : études et mise en place d’une plateforme  e-commerce de dernière génération :**

**AHAMADI NASRY**

Problème :

Vu que la plupart des  entreprises e-commerce font l'ensemble de leurs traitements de données manuellement et n’ont pas une base de données pour sauvegarder leurs informations confidentielles de l’entreprise ce qui engendre un certain nombre de problèmes tels que la lenteur dans l'accès aux données et le risque de perte d'informations et aussi les entreprise qui n’arrive pas à faire le contrôle de leurs informations par exemple savoir les produits qui se trouvent au niveau de l’entreprise et les vente etc… et à la fin ils sont exposés à de nombreuses surprises tels que la fin du stock, vol des produits par les employés ou par des clients sans contrôles ,etc...).  

Solution : 

Pour palier et éviter toutes ces contraintes, j’aimerais créer une plateforme e-commerce qui va permettre de résoudre ces problèmes ensuite gérer les ventes mais aussi gérer l’entreprise de manière à pouvoir aider ce dernier de faire ces traitements de données de manière automatique pour le bon fonctionnement de l’entreprise ,faire le contrôle des informations de manière à pouvoir éviter qu’il ne puisse pas y avoir des vols des produits causés par les employés ou des clients afin de booster les bénéfices et satisfaire la clientèle etc...

Travail à Faire :

Étude et mise en place d’une application de Gestion d’une entreprise (super marcher, pharmacie, boutique, magasin etc…)

- Faire une étude UML en proposant un diagramme de cas d’utilisation et un diagramme de classe.
- Développer une partie back end avec le langage PHP/MySQL.
- Développer une partie front end en utilisant HTML, BOOSTRAP et JAVASCRIPT.

Résultat entendu :

Étude et mise en place d’une plateforme e-commerce.

L’application aura trois interfaces, une interface authentification qui permet à tout personne qui veut se connecter au niveau de l’application puisse s’authentifier d’abord pour des raisons de sécurités une interface admin, une interface employée (comptables).l’interface admin de gérer tout ce qui est (ajouter, modifier, supprimer) des produits, gérer et enregistrer les informations des personnes qui seront recruter dans l’entreprise (par exemple des comptables, des gestionnaires, sécurités etc..), créer des comptes utilisateurs, gérer les fournisseurs des produits gérer les promotions, gérer le stock des produits, faire le bilan des ventes (mensuelle et annuelle).

L’interface réservé pour les comptables ça permet de réaliser des ventes, faire des factures (reçus) de payement et versement de l’argent vendues dans la journée etc…
